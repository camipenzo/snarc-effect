I 29 partecipanti erano divisi in gue gruppi: al gruppo sperimentale era richiesto di usare la mano sisitra per rispondere agli stimoli più piccoli del target e la destra per stimoli più grandi (Z small); al gruppo di controllo era chiesto di usare la mano sinistra per rispondere agli stimoli più grandi del target e la destra per stimoli più piccoli (M small).

Ad ogni partecipante sono stati mostrati 64 stimoli in totale di cui metà più piccoli del target e metà più grandi. I numeri più piccoli e più grandi erano propozionalmente distanti dal target. Vi erano 4 numeri più piccoli e 4 più grandi, ciascuno veniva randomicamente presentato per due volte in una delle 4 notazioni (arabo, hiragana, kanji, letterale).

* La prima ipotesi sperimentale a cui sono interessata riguarda la verifica della presenza dell'effetto SNARC nei miei dati, coerentemente con il modello di riferimento che utilizzava numeri arabi. La procedura è la seguente: \
 la media dei RT del gruppo Z small dovrebbe essere statisticamente inferiore (livello di significatività di 0.05) quando rispondono Z rispetto a quando rispondono M (problema accuratezza e outliers), relativamente al totale degli stimoli e per quanto riguarda gli stimoli arabi \
 H1: media TR con risposta Z < media TR con risposta M \
 H0: media TR con risposta Z >= media TR co risposta M \
 alpha = 0.05 \
 GDL : n-1

* Inoltre l'ipotesi prevede che nel gruppo M small quando i partecipanti rispondono Z la media dei TR non dovrebbe essere statisticamente inferiore rispetto a quando rispondono M (livello di significatività di 0.05), relativamente al totale degli stimoli e per quanto riguarda gli stimoli arabi \
H1: media TR con risposta Z >= media TR con risposta M \
H0: media TR con risposta Z < media TR co risposta M \
alpha = 0.05 \
GDL : n-1

* Infine sono interessata ad osservare se l'effetto SNARC si osserva anche per quanto riguarda le altre notazioni, quindi per ogni notazione eseguire lo stesso procedimento sia per il gruppo M small che Z small
