import math
import numpy

#per ogni partecipante
#dare un nome, aprire e leggere il file in formato csv, ora il file è sottoforma di tabella
#assegno alla variabile gruppo_controllo i partecipanti in cui la colonna task è MsmallZbig, assegno alla variabile gruppo_sperimentale gli altri (i partecipanti in cui la colonna task è ZsmallMbig)
#per ogni partecipante del gruppo_sperimentale, seleziono solo le righe in cui la colonna style è arabic, calcolo la media dei valori RT corrispondenti e la assegno alla variabile media_gruppo_sperimentale
#ora voglio verificare che la differenza tra le due medie sia statisticamente significativa dato un pvalue di 0.05
#deviazione_standard_media = (sqrt(sommatoria di (valori RT arabic gruppo_controllo - media_gruppo_controllo)^2) + sqrt(sommatoria di (valori RT arabic gruppo_sperimentale - media_gruppo_sperimentale)^2)) / 2
#numerosità_gruppo_controllo = 14
#numerosità_gruppo_sperimentale = 13
#gdl = 25
#varianza_ponderata = ...
#la variabile tosservato = (media_gruppo_controllo - media_gruppo_sperimentale) / (sqrt((varianza_ponderata / numerosità_gruppo_controllo) + (varianza_ponderata / numerosità_gruppo_sperimentale)))
#una volta ottenuto il valore t, controllo questo valore con tcritico tabulato relativo ai gdl
#se tosservato < tcritico, stampa 'la differenza non è significativa'
#altrimenti, stampare 'la differenza non è significativa'
